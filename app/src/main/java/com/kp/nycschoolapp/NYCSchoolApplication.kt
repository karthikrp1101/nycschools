package com.kp.nycschoolapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApplication : Application() {
}