package com.kp.nycschoolapp.viewModel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kp.nycschoolapp.data.DataOrException
import com.kp.nycschoolapp.model.SchoolDetail
import com.kp.nycschoolapp.model.SchoolList
import com.kp.nycschoolapp.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {
    val schoolListData: MutableState<DataOrException<SchoolList,
            Boolean, Exception>> = mutableStateOf(DataOrException(null, true, Exception("")))
    val schoolDetailData: MutableState<DataOrException<SchoolDetail,
            Boolean, Exception>> = mutableStateOf(DataOrException(null, true, Exception("")))

    init {
        getAllSchools()
        getSchoolDetail()
    }

    @Singleton
    fun getAllSchools() {
        viewModelScope.launch {
            schoolListData.value.loading = true
            schoolListData.value = repository.fetchSchoolList()

            if (schoolListData.value.data.toString().isNotEmpty()) {
                schoolListData.value.loading = false
            }
        }
    }

    @Singleton
    fun getSchoolDetail() {
        viewModelScope.launch {
            schoolDetailData.value.loading = true
            schoolDetailData.value = repository.fetchSchoolDetails()
        }
        if (schoolDetailData.value.data.toString().isNotEmpty()) {
            Log.d("Kart", "schoolDetails: " + schoolDetailData.value.data?.get(0)?.school_name)
            Log.d(
                "Kart",
                "schoolDetails: " + schoolDetailData.value.data?.get(0)?.sat_critical_reading_avg_score
            )
            schoolDetailData.value.loading = false
        }
    }
}