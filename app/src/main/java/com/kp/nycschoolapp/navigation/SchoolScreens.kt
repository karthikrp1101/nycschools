package com.kp.nycschoolapp.navigation

import kotlin.IllegalArgumentException

enum class SchoolScreens {
    SchoolListScreen,
    SchoolDetailScreen;

    companion object {
        fun fromRoute(route: String?): SchoolScreens = when (route?.substringBefore("/")) {
            SchoolListScreen.name -> SchoolListScreen
            SchoolDetailScreen.name -> SchoolDetailScreen
            null -> SchoolListScreen
            else -> throw IllegalArgumentException("Route $route not present")
        }
    }
}