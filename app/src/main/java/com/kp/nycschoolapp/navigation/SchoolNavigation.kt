package com.kp.nycschoolapp.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.kp.nycschoolapp.ui.SchoolDetailScreen
import com.kp.nycschoolapp.ui.SchoolListScreen

@Composable
fun SchoolNavigation() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = SchoolScreens.SchoolListScreen.name
    ) {
        composable(SchoolScreens.SchoolListScreen.name) {
            SchoolListScreen(navController = navController)
        }

        composable(SchoolScreens.SchoolDetailScreen.name + "/{dbn}",
            arguments = listOf(navArgument(name = "dbn") { type = NavType.StringType })
        ) { backStackEntry ->
            SchoolDetailScreen(
                navController = navController,
                backStackEntry.arguments?.getString("dbn")
            )
        }
    }
}
