package com.kp.nycschoolapp.model

data class SchoolListItem(
    val city: String,
    val dbn: String,
    val phone_number: String,
    val primary_address_line_1: String,
    val school_name: String,
    val website: String,
    val zip: String
)