package com.kp.nycschoolapp.repository

import android.util.Log
import com.kp.nycschoolapp.data.DataOrException
import com.kp.nycschoolapp.model.SchoolDetail
import com.kp.nycschoolapp.model.SchoolList
import com.kp.nycschoolapp.network.SchoolDetailApi
import com.kp.nycschoolapp.network.SchoolListApi
import retrofit2.await
import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private val schoolListApi: SchoolListApi,
    private val schoolDetailApi: SchoolDetailApi
) {
    private val tag: String = this.javaClass.name

    private val schoolListDataOrException = DataOrException<SchoolList,
            Boolean,
            Exception>()

    private val schoolDetailDataOrException = DataOrException<SchoolDetail,
            Boolean,
            Exception>()

    suspend fun fetchSchoolList(): DataOrException<SchoolList, Boolean, Exception> {
        try {
            schoolListDataOrException.loading = true
            schoolListDataOrException.data = schoolListApi.fetchSchoolList().await()
            if (schoolListDataOrException.data!!.size > 0) {
                schoolListDataOrException.loading = false
            }
        } catch (exception: Exception) {
            schoolListDataOrException.e = exception
            Log.d(tag, "Exception in fetchSchoolList: " + exception.message)
        }
        return schoolListDataOrException
    }

    suspend fun fetchSchoolDetails(): DataOrException<SchoolDetail,
            Boolean,
            Exception> {
        try {
            schoolDetailDataOrException.loading = true
            schoolDetailDataOrException.data = schoolDetailApi.getSchoolDetails().await()
            if (schoolDetailDataOrException.data!!.size > 0) {
                schoolDetailDataOrException.loading = false
            }
        } catch (exception: Exception) {
            schoolDetailDataOrException.e = exception
            Log.d(tag, "Exception in fetchSchoolDetails: ")
        }
        return schoolDetailDataOrException
    }
}