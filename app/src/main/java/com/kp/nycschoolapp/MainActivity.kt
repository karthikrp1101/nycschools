package com.kp.nycschoolapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.kp.nycschoolapp.navigation.SchoolNavigation
import com.kp.nycschoolapp.ui.theme.NYCSchoolAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCSchoolAppTheme {
                SchoolNavigation()
            }
        }
    }
}
