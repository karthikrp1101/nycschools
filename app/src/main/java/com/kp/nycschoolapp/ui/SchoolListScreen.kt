package com.kp.nycschoolapp.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.kp.nycschoolapp.model.SchoolListItem
import com.kp.nycschoolapp.navigation.SchoolScreens
import com.kp.nycschoolapp.viewModel.SchoolViewModel
import com.kp.nycschoolapp.R


@Composable
fun SchoolListScreen(navController: NavController) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.White
    ) {
        DisplaySchoolList(navController)
    }
}

@Composable
fun DisplaySchoolList(navController: NavController) {
    Scaffold(topBar = {
        TopAppBar(backgroundColor = Color.White, elevation = 5.dp) {
            Header()
        }
    }, backgroundColor = Color.White) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ListSchools(navController)
        }
    }
}

@Composable
fun Header() {
    Text(
        stringResource(id = R.string.nyc_school_list),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        fontWeight = FontWeight.ExtraBold,
        textAlign = TextAlign.Center,
        fontSize = 30.sp
    )
}


@Composable
fun ListSchools(navController: NavController) {
    val viewModel: SchoolViewModel = hiltViewModel()
    var schoolList = viewModel.schoolListData.value.data?.toList()
    if (schoolList != null) {
        LazyColumn(
            modifier = Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(12.dp)
        ) {
            items(items = schoolList) { schoolItem ->
                CreateCard(schoolItem) { dbn ->
                    navController.navigate(
                        route = SchoolScreens.SchoolDetailScreen.name
                                + "/$dbn"
                    )
                }
            }
        }
    }
}

@Composable
fun CreateCard(
    schoolItem: SchoolListItem,
    onItemClick: (String) -> Unit = {}
) {
    val context = LocalContext.current
    val resources = context.resources
    val displayMetrics = resources.displayMetrics
    val screenWidth = displayMetrics.widthPixels / displayMetrics.density

    Card(
        shape = RoundedCornerShape(10.dp),
        elevation = 10.dp,
        backgroundColor = Color.White,
        modifier = Modifier
            .padding(5.dp)
            .width(screenWidth.dp)
            .clickable {
                onItemClick(schoolItem.dbn)
            },
    ) {
        Column(modifier = Modifier.width(screenWidth.dp)) {
            CreateSchoolName(schoolItem)
            Divider()
            CreateRow(schoolItem.phone_number, Icons.Default.Phone, Color.Red, context)
            CreateRow(schoolItem.website, Icons.Default.Info, Color.Blue, context)
            CreateRow(
                schoolItem.primary_address_line_1 + " " + schoolItem.city + " " + schoolItem.zip,
                Icons.Default.Home,
                Color.Cyan,
                context
            )
        }
    }
}

@Composable
private fun CreateSchoolName(schoolItem: SchoolListItem) {
    Text(
        schoolItem.school_name,
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.Bold,
        color = Color.Black,
        fontSize = 20.sp
    )
}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CreateRow(
    text: String,
    icon: ImageVector,
    color: Color,
    context: Context
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start,
    ) {
        Surface(
            modifier = Modifier
                .padding(12.dp)
                .size(20.dp),
            shape = CircleShape, elevation = 5.dp, onClick = {
                if (icon == Icons.Default.Phone) {
                    try {
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:$text")
                        startActivity(context, intent, null)
                    } catch (exception: Exception) {
                        exception.message
                    }
                } else if (icon == Icons.Default.Info) {
                    try {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://$text"))
                        startActivity(context, intent, null)
                    } catch (exception: Exception) {
                        exception.message
                    }
                } else {
                    try {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(text))
                        startActivity(context, intent, null)
                    } catch (exception: Exception) {
                        exception.message
                    }
                }
            }
        ) {
            Icon(
                imageVector = icon,
                contentDescription = text
            )
        }
        CreateText(text, color)
    }
    Divider()
}

@Composable
fun CreateText(text: String, color: Color) {
    Text(
        text,
        modifier = Modifier.padding(10.dp),
        color = color,
        fontSize = 15.sp
    )
}