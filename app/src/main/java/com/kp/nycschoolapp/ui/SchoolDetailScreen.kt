package com.kp.nycschoolapp.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.kp.nycschoolapp.R
import com.kp.nycschoolapp.model.SchoolDetailItem
import com.kp.nycschoolapp.viewModel.SchoolViewModel

@Composable
fun SchoolDetailScreen(navController: NavController, dbn: String?) {

    Scaffold(topBar = {
        TopAppBar(backgroundColor = Color.White, elevation = 5.dp) {
            Header()
        }
    }, backgroundColor = Color.White) {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val viewModel: SchoolViewModel = hiltViewModel()
            val schoolDetailData = viewModel.schoolDetailData.value.data
            var isMatchFound = false
            Spacer(modifier = Modifier.height(23.dp))
            if (schoolDetailData != null) {
                for (detail in schoolDetailData) {
                    if (detail.dbn == dbn) {
                        isMatchFound = true
                        Card(
                            shape = RoundedCornerShape(10.dp),
                            elevation = 10.dp,
                            backgroundColor = Color.White,
                            modifier = Modifier
                                .padding(5.dp)
                                .fillMaxWidth()
                        ) {
                            Column() {
                                Text(
                                    detail.school_name,
                                    modifier = Modifier
                                        .padding(10.dp)
                                        .fillMaxWidth(),
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold,
                                    color = Color.Black,
                                    fontSize = 20.sp
                                )
                                TableScreen(detail = detail)
                            }
                        }
                    }
                }
            }
            if (!isMatchFound) {
                CreateText(stringResource(id = R.string.no_data), Color.Red)
            }

            Spacer(modifier = Modifier.height(23.dp))
            Button(colors = ButtonDefaults.buttonColors(backgroundColor = Color.Gray), onClick = {
                navController.popBackStack()
            }) {
                Text(text = "Go Back", fontSize = 20.sp)
            }
        }
    }
}

@Composable
fun RowScope.TableCell(
    text: String,
    weight: Float
) {
    Text(
        text = text,
        Modifier
            .border(1.dp, Color.Black)
            .weight(weight)
            .padding(8.dp)
    )
}

@Composable
fun TableScreen(detail: SchoolDetailItem) {
    val column1Weight = .7f
    val column2Weight = .3f

    LazyColumn(
        Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        item {
            Row(Modifier.fillMaxWidth()) {
                TableCell(stringResource(id = R.string.num_sat_test_takers), weight = column1Weight)
                TableCell(text = detail.num_of_sat_test_takers, weight = column2Weight)
            }
            Row(Modifier.fillMaxWidth()) {
                TableCell(
                    stringResource(id = R.string.sat_critical_reading_avg_score),
                    weight = column1Weight
                )
                TableCell(text = detail.num_of_sat_test_takers, weight = column2Weight)
            }
            Row(Modifier.fillMaxWidth()) {
                TableCell(stringResource(id = R.string.sat_math_avg_score), weight = column1Weight)
                TableCell(text = detail.sat_math_avg_score, weight = column2Weight)
            }
            Row(Modifier.fillMaxWidth()) {
                TableCell(
                    stringResource(id = R.string.sat_writing_avg_score),
                    weight = column1Weight
                )
                TableCell(text = detail.sat_writing_avg_score, weight = column2Weight)
            }
        }
    }
}