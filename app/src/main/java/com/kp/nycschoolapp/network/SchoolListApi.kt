package com.kp.nycschoolapp.network

import com.kp.nycschoolapp.model.SchoolList
import retrofit2.Call
import retrofit2.http.GET
import javax.inject.Singleton

@Singleton
interface SchoolListApi {
    @GET("s3k6-pzi2.json")
    fun fetchSchoolList(): Call<SchoolList>

}