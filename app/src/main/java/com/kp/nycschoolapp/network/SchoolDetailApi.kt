package com.kp.nycschoolapp.network

import com.kp.nycschoolapp.model.SchoolDetail
import retrofit2.Call
import retrofit2.http.GET
import javax.inject.Singleton

@Singleton
interface SchoolDetailApi {
    @GET("f9bf-2cp4.json")
    fun getSchoolDetails(): Call<SchoolDetail>
}