package com.kp.nycschoolapp.di

import com.kp.nycschoolapp.network.SchoolDetailApi
import com.kp.nycschoolapp.network.SchoolListApi
import com.kp.nycschoolapp.repository.SchoolRepository
import com.kp.nycschoolapp.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideSchoolRepository(
        api: SchoolListApi, detailApi: SchoolDetailApi
    ) =
        SchoolRepository(api, detailApi)

    @Singleton
    @Provides
    fun provideSchoolApi(): SchoolListApi {
        return Retrofit.Builder()
            .baseUrl(Constants.SCHOOL_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolListApi::class.java)
    }

    @Singleton
    @Provides
    fun provideSchoolDetailApi(): SchoolDetailApi {
        return Retrofit.Builder()
            .baseUrl(Constants.SCHOOL_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolDetailApi::class.java)
    }
}