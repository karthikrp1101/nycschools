- Created app using Jetpack Compose
- Followed MVVM model
- This app contains 2 screens.
    a. List of schools
    b. Details of a particular school
- Tapping on the icon on the list of school will launch the appropriate action like call/view website. Maps integration is not done for address.

